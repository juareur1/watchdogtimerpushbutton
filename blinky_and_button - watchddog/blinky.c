//*************************************************************************************
//This example uses watchdogtimer0 set with a value of 9000000 wich is continually feed 
//untill the push button (SW2) is pressed in this moment the watchdogtimer is not  
//feeding therefore the system is rebooted.
//*************************************************************************************

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "driverlib/debug.h"
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"
#include "driverlib/watchdog.h"
#include "driverlib/rom_map.h"
#include "driverlib/interrupt.h"
#include "driverlib/rom.h"
#include "inc/hw_ints.h"
#include "watchdogtimer0.h"

//*****************************************************************************
//
// The error routine that is called if the driver library encounters an error.
//
//*****************************************************************************
#ifdef DEBUG
void
__error__(char *pcFilename, uint32_t ui32Line)
{
    while(1);
}
#endif

#define SWITCH GPIO_PORTN_BASE 
#define SW2 GPIO_PIN_3  
#define LED_GREEN_STATE GPIO_PIN_7 
//*****************************************************************************//



volatile uint32_t ui32Loop;

extern volatile bool g_bFeedWatchdog0; /* Flag from watchdotimer0*/

int
main(void)
{
    uint32_t value = 0;
    
    
    uint32_t g_ui32SysClock = SysCtlClockFreqSet((SYSCTL_XTAL_25MHZ | SYSCTL_OSC_MAIN | SYSCTL_USE_PLL | SYSCTL_CFG_VCO_480),120000000); 
    
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOQ);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOQ));
    /*Enable GION for SWITCH UP */
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPION);
    GPIOPinTypeGPIOInput(SWITCH, SW2);
    GPIOPadConfigSet(SWITCH ,SW2,GPIO_STRENGTH_2MA,GPIO_PIN_TYPE_STD_WPU);

    /* Enable the GPIO pin for the LED (PQ7).  Set the direction as output, and */
    /* enable the GPIO pin for digital function.*/
   
    GPIOPinTypeGPIOOutput(GPIO_PORTQ_BASE, GPIO_PIN_7);
    GPIOPinWrite(GPIO_PORTQ_BASE, GPIO_PIN_7, 0x0);
    
    /* Enable WDG0 */ 
    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_WDOG0);
    /* Enable the watchdog interrupt.*/
    
    ROM_IntEnable(INT_WATCHDOG);
    
    /* Set the period of the watchdog timer.*/
    
    ROM_WatchdogReloadSet(WATCHDOG0_BASE, 9000000); 
    
    /* Enable reset generation from the watchdog timer.*/
    
    ROM_WatchdogResetEnable(WATCHDOG0_BASE);
    
    /* Enable the watchdog timer.*/
    
    ROM_WatchdogEnable(WATCHDOG0_BASE);

    
    while(1)
    {
        value = GPIOPinRead(SWITCH,SW2);
        if((value & SW2)==0){
                    for(ui32Loop = 0; ui32Loop < 150000; ui32Loop++);
                    if((value & SW2)==0){
                        g_bFeedWatchdog0 = false;
                    }
        }                    
    }
}
