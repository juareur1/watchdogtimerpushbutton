#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "driverlib/debug.h"
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"
#include "driverlib/watchdog.h"
#include "driverlib/rom_map.h"
#include "driverlib/interrupt.h"
#include "driverlib/rom.h"
#include "inc/hw_ints.h"

volatile uint8_t g_ui8CounterWatchdog0 = 0;
volatile bool g_bFeedWatchdog0 = true; /* Flag from watchdotimer0*/

void WatchdogIntHandler(void)
{   
    /* See if watchdog 0 generated an interrupt.*/
    
    if(ROM_WatchdogIntStatus(WATCHDOG0_BASE, true))
    {
        /* If we have been told to stop feeding the watchdog, return immediately */
        /* without clearing the interrupt.  This will cause the system to reset */
        /* next time the watchdog interrupt fires. */
        
        if(g_bFeedWatchdog0 == true)
        {       
            /* Clear the watchdog interrupt.*/
            
            ROM_WatchdogIntClear(WATCHDOG0_BASE);
            
            /* Increment the counter */
            g_ui8CounterWatchdog0++;

            /* Invert the GREEN LED value*/
            GPIOPinWrite(GPIO_PORTQ_BASE, GPIO_PIN_7,
                             (GPIOPinRead(GPIO_PORTQ_BASE,
                                              GPIO_PIN_7) ^
                                              GPIO_PIN_7));      
        }
      
    }
}